""" 
Script to open a pair of output root file from ITSDAQ and create in-/out-put noise and gain plots,
useful for comparing distributions from different charge injections, or biasing configurations.
Will write plots to a root file and pdf. 
"""

import ROOT
import sys

ROOT.gStyle.SetOptStat(0)
ROOT.gStyle.SetLegendBorderSize(0)
ROOT.gStyle.SetLegendFillColor(4000)

def main(argv):

   if len(sys.argv) != 6:
      print('USAGE: %s <input file> <output file>' %(sys.argv[0]))
      sys.exit(1)


   inputFile = sys.argv[1]
   inputFile_b = sys.argv[2]
   inputFile2 = sys.argv[3]
   inputFile2_b = sys.argv[4]
   outputFile = sys.argv[5]

   print("Reading from", inputFile, "writing to", outputFile)

   f = ROOT.TFile.Open(inputFile,"READ")
   f2 = ROOT.TFile.Open(inputFile_b,"READ")
   g = ROOT.TFile.Open(inputFile2,"READ")
   g2 = ROOT.TFile.Open(inputFile2_b,"READ")

#   gain = ROOT.TH1F("gain","Chip gain",128,0,127)
#   outputNoise = ROOT.TH1F("outputNoise","Chip output noise",128,0,127)
#   inputNoise = ROOT.TH1F("inputNoise","Chip input noise",128,0,127)

   gain_1 = f.Get("h_mean0").Clone("gain_1")
   gain_1b = f2.Get("h_mean0").Clone("gain_1b")

   gain_2 = g.Get("h_mean0").Clone("gain_2")
   gain_2b = g2.Get("h_mean0").Clone("gain_2b")
#   gain.Draw()   

   outputNoise_1 = f.Get("h_sigma0").Clone("outputNoise_1")
   outputNoise_2 = g.Get("h_sigma0").Clone("outputNoise_2")
#   outputNoise.Draw()   

   inputNoise_1 = outputNoise_1.Clone("inputNoise_1")
   gain_1.Add(-1*gain_1b)
   inputNoise_1.Divide(gain_1)
   inputNoise_2 = outputNoise_2.Clone("inputNoise_2")
   gain_2.Add(-1*gain_2b)
   inputNoise_2.Divide(gain_2)
#   inputNoise.Draw()

   c1 = ROOT.TCanvas("canvas")
   c1.Print("stackPlots.pdf[")
   c1.Clear()   

   leg = ROOT.TLegend(0.75,0.75,0.88,0.88) 
   leg.AddEntry(inputNoise_1,"V_{bias} = 0 V","ep") 
   leg.AddEntry(inputNoise_2,"V_{bias} = 200 V","ep") 
   leg.SetBorderSize(0) 

   inputNoise_1.SetTitle("Input noise;Channel;Noise")
   inputNoise_1.Draw()
   inputNoise_2.SetLineColor(ROOT.kRed)
   inputNoise_2.Draw("SAME")
   leg.Draw("SAME")
   ROOT.gPad.Update()
   c1.Print("stackPlots.pdf")
   c1.SaveAs("inputNoise.png")
   c1.Clear()   


   outputNoise_1.Draw()
   outputNoise_2.SetLineColor(ROOT.kRed)
   outputNoise_2.Draw("SAME")
   ROOT.gPad.Update()
   c1.Print("stackPlots.pdf")
   c1.Clear()   

   gain_1.Draw()
   gain_2.SetLineColor(ROOT.kRed)
   gain_2.Draw("SAME")
   c1.Print("stackPlots.pdf")
   c1.Print("stackPlots.pdf]")

   outputHistFile = ROOT.TFile.Open(outputFile,"RECREATE")
   outputHistFile.cd()

   gain_1.Write()
   outputNoise_1.Write()
   inputNoise_1.Write()
   f.Close()
   g.Close()
   outputHistFile.Close()

if __name__ == "__main__":
   main(sys.argv)
