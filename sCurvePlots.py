""" 
Script for processing output root file from ITSDAQ and creating s-curve distributions for 
a number of channels.
"""

import ROOT
import sys

ROOT.gStyle.SetOptStat(0)

def main(argv):

   if len(sys.argv) != 3:
      print('USAGE: %s <input file> <output file>' %(sys.argv[0]))
      sys.exit(1)


   inputFile = sys.argv[1]
   outputFile = sys.argv[2]

   print("Reading from", inputFile, "writing to", outputFile)

   f = ROOT.TFile.Open(inputFile,"READ")

#   gain = ROOT.TH1F("gain","Chip gain",128,0,127)
#   outputNoise = ROOT.TH1F("outputNoise","Chip output noise",128,0,127)
#   inputNoise = ROOT.TH1F("inputNoise","Chip input noise",128,0,127)

   scan = f.Get("h_scan0").Clone("Threshold scan")

   scanProj_50 = scan.ProjectionY("50",50,51,"")
   scanProj_80 = scan.ProjectionY("80",80,81,"")
   scanProj_100 = scan.ProjectionY("100",100,101,"")
   
   scanProj_50.SetLineColor(ROOT.kRed)
   scanProj_80.SetLineColor(ROOT.kCyan)
   scanProj_100.SetLineColor(ROOT.kMagenta)

   scanProj_50.SetTitle("Threshold scan [@1.5fC];Threshhold [mV];Eff [arb.]")


   leg = ROOT.TLegend(0.8,0.7,1.0,0.9)
   leg.AddEntry(scanProj_50,"Ch. 50")
   leg.AddEntry(scanProj_80,"Ch. 80")
   leg.AddEntry(scanProj_100,"Ch. 100")


   c1 = ROOT.TCanvas("canvas")
   c1.Print("s-curvePlots.pdf[")
   c1.Clear()   

   scanProj_50.Draw("HIST c")
   scanProj_80.Draw("SAME HIST c")
   scanProj_100.Draw("SAME HIST c")
   leg.Draw()
   ROOT.gPad.Update()
   c1.Print("s-curvePlots.pdf")
   c1.Clear()   

   c1.Print("s-curvePlots.pdf]")

   outputHistFile = ROOT.TFile.Open(outputFile,"RECREATE")
   outputHistFile.cd()

   scan.Write()
   f.Close()
   outputHistFile.Close()

if __name__ == "__main__":
   main(sys.argv)
